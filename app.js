var express = require('express')
	, bodyParser = require('body-parser')
	, cheerio = require('cheerio')
	, request = require('request')
	, fs = require('fs')
	, app = express()
	;

var URL_LIST = ['http://techcrunch.com/']
	, SEARCH_STRING = 'year'
	, MAX_LINKS_PER_URL = 100
	, pagesVisited = 0
	;

app.engine('html',require('ejs').renderFile);
app.set('views', __dirname + '/views');
app.set('view engine ', 'html');


app.use(function(req, res, next) {
	res.on('data', function(chunk) {
		console.log("chunk", data)
	});
	next();
})
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.get('/', (req, res) => {
	console.log("You are at", req.url);
	res.render('template.html');
	//crawl(req, res);
	
})

app.post('/', (req, res) => {
	console.log("post", req.body)
	crawl(req, res);
});

function setUrlList(arr) {
	URL_LIST = arr;
	return;
}

function crawl(req, res) {
	var MAX_ASYNC_CALLS = 10
		;

	searchAllUrls(res, URL_LIST, function(docs) {

		//docs should be in JSON here (maybe?)
		//res.json(docs);

	}, MAX_ASYNC_CALLS);
}

function searchAllUrls(res, urls, cb, maxAsync) {
	//if maxAsync is null, do 1 at a time
	maxAsync = maxAsync || 1;//we dont want this to be a falsy value by any means
	var docs = [];
	for(var i = 0; i < urls.length; i++) {
		docs.push(visitUrl(res, urls[i]));
	}
	
	if(typeof(cb) === 'function')
		cb(docs)
}

function visitUrl(res, url) {
	console.log("url: ", url)
	request(url, function(error, response, body) {
		// Check status code (200 is HTTP OK)
		console.log("Status code: " + response.statusCode);
		if(response.statusCode !== 200) {
			callback();
			return;
		}
		// Parse the document body
		var $ = cheerio.load(body)
			, itemsFound = 0
			, objFound = []
			, obj = {}
			;

		obj.techCrunch = [];
		$('div[role="main"] li.river-block').filter(function(index){
			var data = $(this)
				, sotryText = data.text().toLowerCase()
				, isFound =  (sotryText.indexOf(SEARCH_STRING.toLowerCase()) !== -1)
				;

			
			if(isFound) {
				var o = {}

				o.title = data.attr("data-sharetitle");
				o.link = data.attr("data-shortlink");
				o.teaser = data.find('.block-content').find('p.excerpt').text();
				o.image = data.find('.block-content').find('span').find('.thumb').find('img').attr('src')
				itemsFound++;
				obj.techCrunch.push(o);
				console.log("Found", itemsFound, index);
			} else {
				console.log("Not Found in ", url, index)
			}
    	});
    	objFound.push(obj);
    	console.log("Sending: ", objFound[0])
		res.json({
			data: objFound
		});
    	console.log("END")
    });
}

function renderDataToView(docs, cb) {

}

app.listen(3030, function(){ console.log("Listening on 3030")});