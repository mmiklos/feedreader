#Feed Reader
##Learning Project

The purpose of this project is to, using node.js, cheerio, request and vanilla javascript as the main dependencies, build a mobile layout feed reader by searching through sites for key words and returning their summarized data in json to the server. Then that json is rendered on the page using ejs templating.